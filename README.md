This project is deployed on [Heroku](https://vast-cliffs-87331.herokuapp.com/home)

## Features
1. [Create React App](https://github.com/facebookincubator/create-react-app).

2. React Redux

3. React Router v4

4. Redux Thunk

5. React Bootstrap

6. Simple CRUD operation

## Requirements

1. Node >= 6 NPM >= 3

2. [node-api](https://gustavo_lz@bitbucket.org/gustavo_lz/node-todo-api.git)

## Start the project

1. Install node modules `npm install`

2. Start the project `npm start`
